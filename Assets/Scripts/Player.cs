﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	public GUIText scoreGUITexture;
	public AudioClip soundPip;
	private int score = 0;

	private void OnGUI()
	{
		string scoreMessage = "Score: " + score;
		scoreGUITexture.text = scoreMessage;
	}

	private void OnTriggerEnter(Collider c)
	{
		if(c.CompareTag("Finish"))
		{
			score++;
			audio.PlayOneShot(soundPip);
		}
	}

}
